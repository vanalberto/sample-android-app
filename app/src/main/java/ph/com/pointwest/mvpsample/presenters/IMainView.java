package ph.com.pointwest.mvpsample.presenters;

import java.util.ArrayList;

import ph.com.pointwest.mvpsample.objects.Deck;

public interface IMainView {
    void onSuccessfulResponse(Deck deck);
}
