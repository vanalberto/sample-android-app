package ph.com.pointwest.mvpsample.presenters;

/**
 * Created by reemer.bundalian on 13, August, 2019
 */
public interface ILoginView {
    void onSuccessfulLogin(String message);
    void onFailedLogin(String message);
}
