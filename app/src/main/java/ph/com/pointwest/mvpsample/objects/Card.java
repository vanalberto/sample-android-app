package ph.com.pointwest.mvpsample.objects;

public class Card {
    private String image;

    public String getImage(){
        return image;
    }

    private String value;

    public String getValue(){
        return value;
    }

    private String suit;

    public String getSuit(){
        return suit;
    }

    private String code;

    public String getCode(){
        return code;
    }

}
