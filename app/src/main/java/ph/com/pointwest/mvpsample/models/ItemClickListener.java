package ph.com.pointwest.mvpsample.models;

import android.view.View;

import ph.com.pointwest.mvpsample.objects.Card;

public interface ItemClickListener {
    //view on which user clicked and the post_id for which you want to
    //some specific action
    void onTextClick(View itemView, int post_id);
    void onImageClick(View itemView, int post_id);
}
