package ph.com.pointwest.mvpsample.modules.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import ph.com.pointwest.mvpsample.R;
import ph.com.pointwest.mvpsample.objects.Credential;
import ph.com.pointwest.mvpsample.presenters.ILoginPresenter;
import ph.com.pointwest.mvpsample.presenters.ILoginView;
import ph.com.pointwest.mvpsample.presenters.LoginPresenter;

public class LoginFragment extends Fragment implements ILoginView {
    private ILoginPresenter iLoginPresenter;
    private View view;
    private EditText etEmail;
    private EditText etPassword;
    private Button loginBtn;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);
        etEmail = view.findViewById(R.id.edt_login_username);
        etPassword = view.findViewById(R.id.edt_login_password);

        loginBtn = view.findViewById(R.id.btn_login);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLogin(view);
            }
        });


        return view;
    }

    public void onSuccessfulLogin(String message) {
        //return value from ILoginView
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        MainFragment mainFragment = new MainFragment(etEmail.getText().toString());

        Fragment loginFragment = fragmentManager.findFragmentById(R.id.fragment_container);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_container, mainFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void onFailedLogin(String message) {
        //return value from ILoginView
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void onLogin(View view) {
        //Call onLogin from PRESENTER including credential params.
        iLoginPresenter.onLogin(setCredential());
    }

    private void init() {
        iLoginPresenter = new LoginPresenter(this);
    }

    private Credential setCredential() {
        /**
         * setting credential object's value by getting etEmail and etPassword values.
         * since etEmail and etPassword is already global (inside its own activity only)
         * you can directly call it anywhere inside the activity
         */
        Credential credential = new Credential();
        credential.setEmail(etEmail.getText().toString().trim());
        credential.setPassword(etPassword.getText().toString().trim());
        return credential;
    }
}
