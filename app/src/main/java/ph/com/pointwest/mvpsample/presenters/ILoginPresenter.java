package ph.com.pointwest.mvpsample.presenters;

import ph.com.pointwest.mvpsample.objects.Credential;

/**
 * Created by reemer.bundalian on 13, August, 2019
 */
public interface ILoginPresenter {
    void onLogin(Credential credential);
}
