package ph.com.pointwest.mvpsample.modules.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ph.com.pointwest.mvpsample.R;
import ph.com.pointwest.mvpsample.objects.Card;

/**
 * A simple {@link Fragment} subclass.
 */
public class CardFragment extends Fragment {
    private View view;
    private Card selectedCard;

    public CardFragment() {
    }

    public CardFragment(Card selectedCard) {
        this.selectedCard = selectedCard;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_card, container, false);

        ImageView cardImageView = view.findViewById(R.id.cardImageView);
        Picasso.get().load(selectedCard.getImage()).into(cardImageView);

        TextView suitTextView = view.findViewById(R.id.suitTextView);
        suitTextView.setText("Suit: " + this.selectedCard.getSuit());
        TextView valueTextView = view.findViewById(R.id.valueTextView);
        valueTextView.setText("Value: " + this.selectedCard.getValue());


        return view;
    }

}
