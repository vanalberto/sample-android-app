package ph.com.pointwest.mvpsample.models;

import android.util.Log;

import org.json.JSONObject;

import ph.com.pointwest.mvpsample.apis.Api;
import ph.com.pointwest.mvpsample.objects.Credential;
import ph.com.pointwest.mvpsample.objects.Token;
import ph.com.pointwest.mvpsample.presenters.ILoginModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * Created by reemer.bundalian on 13, August, 2019
 *
 * In MVP, this is the MODEL part. this is where you connect your APP to an API/Server
 * it gets data from API, then pass it to PRESENTER
 * you can also add some small logics here to ease data transfer
 */
public class LoginModel {

    private ILoginModel iLoginModel;

    public LoginModel(ILoginModel iLoginModel) {
        this.iLoginModel = iLoginModel;
    }

    public void loginAccount(final Credential credential) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        Call<Token> call = api.login(credential);

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.code() == 200) {
                    Log.e("login", "log");
                    iLoginModel.onSuccess(response.body().getToken());
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String err = jObjError.getString("error");
                        iLoginModel.onFail(err);
                    } catch (Exception e) {
                        iLoginModel.onFail(e.getMessage());
                    }

                    iLoginModel.onFail("failed");
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                iLoginModel.onFail("failed");
            }
        });
    }
}
