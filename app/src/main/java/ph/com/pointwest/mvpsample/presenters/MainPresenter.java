package ph.com.pointwest.mvpsample.presenters;

import java.util.ArrayList;

import ph.com.pointwest.mvpsample.models.MainModel;
import ph.com.pointwest.mvpsample.objects.Deck;

public class MainPresenter implements IMainPresenter, IMainModel {
    private IMainView iMainView;

    public MainPresenter(IMainView mainView){
        this.iMainView = mainView;
    }

    @Override
    public void loadDataFromAPI(){
        MainModel mainModel = new MainModel(this);
        mainModel.loadCards();
    }

    @Override
    public void onSuccessfulResponse(Deck deck) {
        iMainView.onSuccessfulResponse(deck);
    }
}
