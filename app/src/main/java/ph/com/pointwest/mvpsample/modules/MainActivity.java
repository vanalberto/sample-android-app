package ph.com.pointwest.mvpsample.modules;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ph.com.pointwest.mvpsample.R;
import ph.com.pointwest.mvpsample.modules.fragments.LoginFragment;
import ph.com.pointwest.mvpsample.modules.fragments.MainFragment;
import ph.com.pointwest.mvpsample.objects.Credential;
import ph.com.pointwest.mvpsample.presenters.ILoginPresenter;
import ph.com.pointwest.mvpsample.presenters.ILoginView;
import ph.com.pointwest.mvpsample.presenters.LoginPresenter;

/**
 * Created by reemer.bundalian on 13, August, 2019
 *
 * BASIC AUTHENTICATION/LOGIN IN MVP ARCHITECTURE
 *
 *
 * In MVP, this is the VIEW part. this is where your UI is display
 * it only fetch/get data from your PRESENTER.
 */
public class MainActivity extends FragmentActivity {
    private ILoginPresenter iLoginPresenter;
    private EditText etEmail;
    private EditText etPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        LoginFragment loginFragment = new LoginFragment();

        fragmentTransaction.add(R.id.fragment_container, loginFragment);
        fragmentTransaction.commit();
    }

}
