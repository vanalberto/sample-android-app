package ph.com.pointwest.mvpsample.apis;

import ph.com.pointwest.mvpsample.objects.Credential;
import ph.com.pointwest.mvpsample.objects.Token;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by reemer.bundalian on 08, August, 2019
 */
public interface Api {
    String BASE_URL = "https://reqres.in/";

    @POST("api/login")
    Call<Token> login(@Body Credential credential);
}
