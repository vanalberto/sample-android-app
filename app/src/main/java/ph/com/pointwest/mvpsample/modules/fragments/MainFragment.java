package ph.com.pointwest.mvpsample.modules.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ph.com.pointwest.mvpsample.R;
import ph.com.pointwest.mvpsample.models.ItemClickListener;
import ph.com.pointwest.mvpsample.modules.adapters.SampleAdapter;
import ph.com.pointwest.mvpsample.objects.Card;
import ph.com.pointwest.mvpsample.objects.Deck;
import ph.com.pointwest.mvpsample.presenters.IMainPresenter;
import ph.com.pointwest.mvpsample.presenters.IMainView;
import ph.com.pointwest.mvpsample.presenters.MainPresenter;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements IMainView, ItemClickListener {
    private View view;
    SampleAdapter sampleAdapter;
    private String userEmail;
    private IMainPresenter iMainPresenter;
    private ArrayList<Card> cards;

    public MainFragment(){

    }

    public MainFragment(String userEmail) {
        // Required empty public constructor
        this.userEmail = userEmail;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main, container, false);
        TextView mainFragmentTextView = view.findViewById(R.id.main_fragment_textview);
        mainFragmentTextView.setText("Hello " + this.userEmail);

        if(cards != null){
            RecyclerView recyclerView = view.findViewById(R.id.recyclerViewMain);
/*
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
*/
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
            sampleAdapter = new SampleAdapter(cards);
            sampleAdapter.setClickListener(this);
            recyclerView.setAdapter(sampleAdapter);
        }

        return view;
    }

    private void init() {
        iMainPresenter = new MainPresenter(this);
        loadRecyclerView();
    }

    public void loadRecyclerView(){
        iMainPresenter.loadDataFromAPI();
    }

    public void onSuccessfulResponse(Deck deck){
        cards = deck.getCards();

        Log.e("deck", cards.get(0).getSuit());
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewMain);
/*
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
*/
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        sampleAdapter = new SampleAdapter(cards);
        sampleAdapter.setClickListener(this);
        recyclerView.setAdapter(sampleAdapter);

    }


    @Override
    public void onTextClick(View itemView, int position) {
        Log.e("sampleClick", "click");
        Toast.makeText(getActivity(), "You clicked " + sampleAdapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onImageClick(View itemView, int position) {
        Log.e("sampleClick", "click card");

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Card selectedCard = sampleAdapter.getCard(position);
        CardFragment cardFragment = new CardFragment(selectedCard);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, cardFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

}
