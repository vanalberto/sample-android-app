package ph.com.pointwest.mvpsample.objects;

/**
 * Created by reemer.bundalian on 08, August, 2019
 */
public class Token {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
