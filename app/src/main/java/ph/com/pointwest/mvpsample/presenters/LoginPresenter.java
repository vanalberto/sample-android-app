package ph.com.pointwest.mvpsample.presenters;

import android.content.Context;
import android.util.Log;
import android.util.Patterns;

import ph.com.pointwest.mvpsample.models.LoginModel;
import ph.com.pointwest.mvpsample.objects.Credential;

/**
 * Created by reemer.bundalian on 13, August, 2019
 *
 * In MVP, this is the PRESENTER part. this is where you add your logic/business rules.
 * it gets data from your MODEL, filter/process it here, then return a result
 * in your VIEW part.
 */
public class LoginPresenter implements ILoginPresenter, ILoginModel {
    private ILoginView iLoginView;

    /*
     * you need this to connect MainActivity and ILoginView
     * since you are going to user ILoginView methods here.
     */
    public LoginPresenter(ILoginView iLoginView) {
        this.iLoginView = iLoginView;
    }

    @Override
    public void onLogin(Credential credential) {
        /*
         * you can add here your filters before going to API call.
         * you can save steps here.
         */
        if (credential.getEmail().equals("")) {
            iLoginView.onFailedLogin("Please provide email address");
            return;
        }

        if (credential.getPassword().equals("")) {
            iLoginView.onFailedLogin("Please provide password");
            return;
        }

        if (credential.getPassword().length() < 6) {
            iLoginView.onFailedLogin("Password should be 6 characters and above.");
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(credential.getEmail()).matches()) {
            iLoginView.onFailedLogin("Invalid email address format.");
            return;
        }

        /**
         * after your initial filters passed. you can now go to your MODEL to fetch
         * your API.
         */
       LoginModel loginModel = new LoginModel(this);
       loginModel.loginAccount(credential);
    }

    @Override
    public void onSuccess(String message) {
        iLoginView.onSuccessfulLogin("Successful Login");
    }

    @Override
    public void onFail(String message) {
        iLoginView.onFailedLogin("Failed Login");
    }
}
