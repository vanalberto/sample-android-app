package ph.com.pointwest.mvpsample.presenters;

/**
 * Created by reemer.bundalian on 13, August, 2019
 */
public interface ILoginModel {
    void onSuccess(String message);
    void onFail(String message);
}
