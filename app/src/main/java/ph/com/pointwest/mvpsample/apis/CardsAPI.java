package ph.com.pointwest.mvpsample.apis;

import ph.com.pointwest.mvpsample.objects.Deck;
import ph.com.pointwest.mvpsample.objects.Token;
import retrofit2.Call;
import retrofit2.http.GET;

public interface CardsAPI {
    String BASE_URL = "https://deckofcardsapi.com/";

    @GET("https://deckofcardsapi.com/api/deck/new/draw/?count=7")
    Call<Deck> getDeck();
}
