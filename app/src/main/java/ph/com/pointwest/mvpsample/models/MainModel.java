package ph.com.pointwest.mvpsample.models;

import android.util.Log;

import org.json.JSONObject;

import ph.com.pointwest.mvpsample.apis.Api;
import ph.com.pointwest.mvpsample.apis.CardsAPI;
import ph.com.pointwest.mvpsample.objects.Deck;
import ph.com.pointwest.mvpsample.objects.Token;
import ph.com.pointwest.mvpsample.presenters.IMainModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainModel {
    private IMainModel iMainModel;

    public MainModel(IMainModel iMainModel) {
        this.iMainModel = iMainModel;
    }

    public void loadCards(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CardsAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CardsAPI api = retrofit.create(CardsAPI.class);
        Call<Deck> call = api.getDeck();

        call.enqueue(new Callback<Deck>() {
            @Override
            public void onResponse(Call<Deck> call, Response<Deck> response) {
                Log.e("sample", response.toString());
                if (response.code() == 200) {
                    Log.e("sample", response.message());
                    iMainModel.onSuccessfulResponse(response.body());
                } else {
                    try {
                        Log.e("sample", response.message());
                    } catch (Exception e) {

                    }
                }
            }
            @Override
            public void onFailure(Call<Deck> call, Throwable t) {
                Log.e("sample", t.getMessage());
            }
        });
    }

}
