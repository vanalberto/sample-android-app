package ph.com.pointwest.mvpsample.modules.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ph.com.pointwest.mvpsample.R;
import ph.com.pointwest.mvpsample.models.ItemClickListener;
import ph.com.pointwest.mvpsample.objects.Card;

public class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.ViewHolder>{
    private List<Card> mData;
    // Define listener member variable
    private ItemClickListener mClickListener;


    // data is passed into the constructor
    public SampleAdapter( ArrayList<Card> data) {
        this.mData = data;
    }

    // inflate the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_row, viewGroup, false);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String code = mData.get(position).getValue() + " of " + mData.get(position).getSuit();
        Picasso.get().load(mData.get(position).getImage()).into(holder.imageView);

        holder.textView.setText(code);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public String getItem(int id) {
        String cardValue = mData.get(id).getValue() + " of " + mData.get(id).getSuit();

        return cardValue;
    }

    public Card getCard(int id) {
        Card selectedCard = mData.get(id);

        return selectedCard;
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.recyclerRowText);
            imageView = itemView.findViewById(R.id.recyclerRowImg);
            textView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onTextClick(view, getAdapterPosition());
                    }
                }
            });

            imageView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (mClickListener != null){
                        mClickListener.onImageClick(view, getAdapterPosition());
                    }
                }
            });

        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

}
